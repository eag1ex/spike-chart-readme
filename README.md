## SpikeChart App

#### Overview
This is a charting application,  renders chart from stocks data  min/average/max data-set range, using dynamic api search feature. Sidebar displays latest news sections which can be viewed in popup modal. You can post latest feeds with rating, and you can search for full and partial match.

#### Team
Project was a small team afford, 1 DE, 1 FE, PM, QA and UAT.
We used Agile more less..

#### Deadline
Initial deadline was for 2 months but was extended due to BE implementation changes and client requests.

#### Application
APP was build using **Angular 6 CLI** as part of larger project divided into smaller apps, which allowed reuse and share of components and services.
##### Chart.j
Integration of **Chart.js** into application required producing class models for managing individual stock multi-set items, and http error handling of dirty data.
##### Search
Search feature works with full and partial match for local results (at this stage)


#### STACK
FE: APP runs on Angular 6 CLI, TS. with RX.SJ, states, nrwl (nx) schemes, Interfaces, PROXY, environments (dev, uat, release), authentication, Chart.js,resolvers,guards, utils,Moment, Mock-Back-end Server, Playground (testing), ServiceWorker.

#### BUILD
Jenkins > Bitbucket > PR (Branch code review) > deploy.
BE: Microsoft Azure > Swagger
 
#### Project requirements
Project had functional spec and In-vision ux/ui, wasn't always maintained.


#### REST/API
The App was integrated with rest api for azure microsoft/ swagger. But i cannot show real live appication since its private/proprietary, all live api's have been replaced with Mock-Back-end on client, serving static data, for  stocks/ api. I have replaced original news api with a developer livense api from `https://newsapi.org` and serving it from my node.js server on Heroku.

#### Project sign-off
*Project and client are private/proprietary*, I have updated UX/UI and context. Real REST/API is authenticated for restricted users only. Now using Mock-Back-end Server on client, and simulate real REST/API calls with static data.

### Demo build:
The demo project is being hosted on heroku/node behing gateway login:

```sh
https://mysterious-hollows-67349.herokuapp.com/login
login:spike
password:spike
# the session has time out
```


## Project Structure

The project separated into 3 different apps

| App       | Detail                                               |
| --------- | ---------------------------------------------------- |
| dashboard | Dashboard for Quotation Engine                       |
| admin     | Administration panel, manage user, logs and settings |
| si        | System integtration for data importing               |

### Library

Library is components or services commonly used in one or more projects

| Library                    | Detail                                                                                         |
| -------------------------- | ---------------------------------------------------------------------------------------------- |
| @spike/core/front           | Modules, components used in login and forgotpassword                              
                                         |
                                           |
| @spike/theme/front          | Theme for front door contains dump components for ui element eg. layout, card, button etc.     |
| @spike/theme/dashboard      | Theme for all dashboard, contains dump components for ui element eg. layout, card, button etc. |
| @spike/http                 | Services consume HTTP                                                                          |
| @spike/guards               | Route guards                                                                                   |
| @spike/states               | NGRX or stateful service                                                                       |

## Basic Usage

For development, just clone the project, install dependencies and start development server

```sh
git clone git@bitbucket.org:repo
cd spike-dashboard-frontend
npm install
npm run start
```

By default, when you run `npm run start` or `npm start`, it will start `dashboard` app.

To start specific app, you can choose from the following snippets

```sh
npm run start:dashboard
npm run start:admin
npm run start:si
```

This also apply to production code building

```sh
npm run build:dashboard
npm run build:admin
npm run build:si
```

## Code Editor Integration

Consider configuring your code editor to have support for compatible formatter and linter.

### Vitual Studio Code

`.vscode/settings.json` is shipped along with the project, contains neccessary setup ready to use with the project

```json
{
  "files.exclude": {
    "**/.git": true,
    "**/.svn": true,
    "**/.hg": true,
    "**/CVS": true,
    "**/.DS_Store": true,
    "**/admin-e2e": true,
    "**/si-e2e": true,
    "**/dashboard-e2e": true
  },
  "[typescript]": {
    "editor.formatOnSave": true
  },
  "[scss]": {
    "editor.formatOnSave": true
  },
  "prettier.tslintIntegration": true,
  "prettier.stylelintIntegration": true
}
```

Required Extensions

- Prettier - https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
- Stylelint - https://marketplace.visualstudio.com/items?itemName=shinnn.stylelint
- Tslint - https://marketplace.visualstudio.com/items?itemName=eg2.tslint
- Angular Language Service - https://marketplace.visualstudio.com/items?itemName=Angular.ng-template

## Creator Note

Command used when scaffolding project.

```sh
create-nx-workspace spike --npm-scope spike
ng g app dashboard --style scss --routing
ng g app admin --style scss --routing
ng g app si --style scss --routing
ng g lib front --directory core
ng g lib spikechart-name --directory core
ng g lib front --directory theme
ng g lib dashboard --directory theme
ng g lib http
ng g lib guards
ng g lib states
ng add @angular/pwa --project dashboard
ng add @angular/pwa --project admin
ng add @angular/pwa --project si
```

```sh
comp

ng g app comp
ng g app spikechart-admin
create folder spikechart-admin at /libs/
create folder comp at /libs/
ng g lib pages --directory=comp --routing
ng g lib pages --directory=spikechart-admin --routing
ng g lib http --directory=comp
ng g lib http --directory=spikechart-admin
ng g lib interfaces --directory=comp
ng g lib interfaces --directory=spikechart-admin
ng g lib services --directory=comp
ng g lib services --directory=spikechart-admin
ng g lib components --directory=comp --routing
ng g lib components --directory=spikechart-admin --routing
ng g lib states --directory=comp
ng g lib states --directory=spikechart-admin
ng g lib resolvers --directory=comp
ng g lib resolvers --directory=spikechart-admin

ng g c main --project=comp
ng g c main --project=spikechart-admin

package.json
start:spikechart-a: ng serve spikechart-admin --proxy-config proxy/spikechart-admin.json,
start:spikechart: ng serve spikechart --proxy-config proxy/comp.json,

generate user module
ng g m users --project=spikechart-admin-pages --routing
ng g c user-list --project=spikechart-admin-pages --routing

ng g m upload --project=spikechart-pages --routing
ng g c upload --project=spikechart-pages --routing

ng g m validation --project=spikechart-admin-pages --routing
ng g c file-list --project=spikechart-admin-pages --module=validation --routing
ng g c generate-result --project=spikechart-admin-pages --module=validation --routing
ng g c benchmark-source --project=spikechart-admin-pages --module=validation --routing
ng g c tender --project=spikechart-pages

ng g m reports --project=spikechart-pages --routing

ng g c reports/team --project=spikechart-pages --routing
ng g c reports/priceModel --project=spikechart-pages --routing
ng g c reports/exportToPims --project=spikechart-pages --routing
ng g c reports/pimsResult --project=spikechart-pages --routing
ng g c reports/cip --project=spikechart-pages --routing
ng g c reports/exportToCrude --project=spikechart-pages --routing
ng g c reports/tabMenu --project=spikechart-pages --routing

ng g c productManagement --project=spikechart-admin-pages --routing
ng g c crudeManagement --project=spikechart-admin-pages --routing

ng g m scheduling --project=spikechart-pages --routing
ng g c scheduling/crude-stock --project=spikechart-pages --routing
ng g c scheduling/uploads --project=spikechart-pages --routing
ng g c scheduling/main-scheduling --project=spikechart-pages --routing
```

## How to lazy load module across libs in NX

```sh
1. Export all lazy modules in index.ts files. (*important)
2. Add references to index.ts files in app-level tsconfig.app.json. Pay attention - not in root tsconfig.json, but in app/tsconfig.app.json;
3. Make sure you dont import lazy modules anywhere.
4. implement it like this loadChildren: '@spike/spikechart/pages#ReportsModule'
```

```sh
COMP Sidebar refactoring
ng g c sidebar-new --project=theme-spikechart
ng g lib interfaces --directory=spikechart

```
